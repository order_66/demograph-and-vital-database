DROP ROLE IF EXISTS "patientSimulator_App";

CREATE ROLE "patientSimulator_App" PASSWORD 'gVq)(4cdAX:zfi57' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;
GRANT
    SELECT
    ON ALL TABLES IN SCHEMA "patientsimulator" TO "patientSimulator_App";
    
GRANT
    SELECT
    ON ALL TABLES IN SCHEMA "management" TO "patientSimulator_App";

	-- grant on user tables
GRANT 
	INSERT, UPDATE, DELETE
	ON TABLE "management"."user" TO "patientSimulator_App";
	
GRANT 
	INSERT, UPDATE, DELETE
	ON TABLE "management"."personalized_data" TO "patientSimulator_App";

GRANT 
	INSERT, UPDATE, DELETE
	ON TABLE "management"."company_data" TO "patientSimulator_App";

GRANT 
	INSERT, UPDATE, DELETE
	ON TABLE "management"."person_data" TO "patientSimulator_App";

    -- grant on vitalparameter tables
GRANT
    INSERT, DELETE
    ON TABLE "patientsimulator"."continuous_vitalparameter" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."reference_vitalparameter" TO "patientSimulator_App";
    
    -- grant on organ tables
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."organs" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."capillary_blood_organ" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."lung_organ" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."body_organ" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."peripheral_vein_organ" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."peripheral_artery_organ" TO "patientSimulator_App";
    
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."venous_blood_organ" TO "patientSimulator_App";
    
    -- Grant on patient table
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."patient" TO "patientSimulator_App";
    
    -- grant on icd10 tables
GRANT
    INSERT, UPDATE, DELETE
    ON TABLE "patientsimulator"."alement_of_icd10" TO "patientSimulator_App";
    
    -- Do not grant on physical_status_icd10 or impairing_physical_change yet, because the entries have to be approved by the ELGA reference system (later grant on an "Administration" user, who might also overrule the patient.is_alterable statement