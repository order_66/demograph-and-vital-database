--create extension if not exists "uuid-ossp" SCHEMA "public";
--SET search_path = "public";

--CREATE SCHEMA IF NOT EXISTS "management";

DROP TABLE IF EXISTS "management"."version" CASCADE;
CREATE TABLE "management"."version" (
  "uuid" uuid NOT NULL,
  "major_version" int NOT NULL,
  "minor_version" int NOT NULL,
  "is_alpha" boolean NOT NULL,
  "is_beta" boolean NOT NULL,
  "published_at" date DEFAULT NULL,
  PRIMARY KEY ("uuid")
);
INSERT INTO "management"."version" ("uuid", "major_version", "minor_version", "is_alpha", "is_beta", "published_at") 
VALUES ('45B81489-67C8-11EA-B4E5-98E7F42E1CCF',2,1,'1','0','2020-03-16'),
	('8FFB4013687011EAB4E598E7F42E1CCF',2,2,'1','0','2020-03-18'),
	('9F44EEAB861A11EAA66F98E7F42E1CCF',3,1,'1','0','2020-04-24'),
	('802cb1e4-1f51-11eb-a018-0f53b0829a0f',4,1,'1','0','2020-11-01'),
	('c4b40a6a-1f51-11eb-a019-4325bf7e3108',4,2,'1','0','2020-11-07'),
	('25e1d8ea-3aea-11eb-a035-3747f8066038',4,3,'0','1','2020-12-10');

DROP TABLE IF EXISTS "management"."viewer_rights" CASCADE;
CREATE TABLE "management"."viewer_rights" (
	"id" int,
	"description" varchar(15) NOT NULL,
	PRIMARY KEY ("id")
);
INSERT INTO "management"."viewer_rights" ("id", "description") VALUES (0, 'Hidden'),(1, 'Read only'),(2, 'Modification');

DROP TABLE IF EXISTS "management"."personalized_data" CASCADE;
CReATE TABLE "management"."personalized_data"(
	"uuid" uuid,
	PRIMARY KEY ("uuid")
);
INSERT INTO "management"."personalized_data" ("uuid") VALUES ('0f833e22-1f65-11eb-a020-e7c3a0389279'),('1dfccde2-1f65-11eb-a021-6701e091d585');

DROP TABLE IF EXISTS "management"."company_data" CASCADE;
CREATE TABLE "management"."company_data"(
	"uuid" uuid,
	"name" varchar(60) DEFAULT NULL,
	"address" varchar(60) DEFAULT NULL,
	"contact_email" varchar(60) DEFAULT NULL,
	"webaddress" varchar(60) DEFAULT NULL,
	PRIMARY KEY ("uuid"),
	CONSTRAINT "company_data_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "management"."personalized_data" ("uuid") ON DELETE CASCADE
);
INSERT INTO "management"."company_data" ("uuid","name","address","contact_email","webaddress") VALUES ('0f833e22-1f65-11eb-a020-e7c3a0389279', 'Vienna Interdisziplinary Professions', NULL, NULL, NULL);

DROP TABLE IF EXISTS "management"."person_data" CASCADE;
CREATE TABLE "management"."person_data" (
	"uuid" uuid,
	"firstname" varchar(60) DEFAULT NULL,
	"lastname" varchar(60) DEFAULT NULL,
	"titles" varchar(60) DEFAULT NULL,
	"birthdate" date DEFAULT NULL,
	"email" varchar(60) DEFAULT NULL,
	"nationality_code" varchar(3) DEFAULT NULL,
	"employer" varchar(60) DEFAULT NULL,
	PRIMARY KEY ("uuid"),
	CONSTRAINT "person_data_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "management"."personalized_data" ("uuid") ON DELETE CASCADE
);
INSERT INTO "management"."person_data" ("uuid", "firstname", "lastname", "titles", "birthdate", "email", "nationality_code", "employer") VALUES ('1dfccde2-1f65-11eb-a021-6701e091d585', 'Donna Syvil', 'Belluci', 'Dr. med.', '1971-04-23', NULL, 'ITA', 'Salvator Mundi International Hospital');

DROP TABLE IF EXISTS "management"."user" CASCADE;
CREATE TABLE "management"."user" (
	"uuid" uuid,
	"username" varchar(32) UNIQUE NOT NULL,
	"password_hash" varchar(64) NOT NULL,
	"pwd_salt" varchar(12) NOT NULL,
	"insurance_number_hash" varchar(64) UNIQUE,
	"user_personell_uuid" uuid DEFAULT NULL,
	PRIMARY KEY ("uuid"),
	CONSTRAINT "user_ibfk_1" FOREIGN KEY ("user_personell_uuid") REFERENCES "management"."personalized_data" ("uuid") ON DELETE CASCADE
);
INSERT INTO "management"."user" ("uuid", "username", "password_hash", "pwd_salt", "insurance_number_hash", "user_personell_uuid") VALUES ('78d72a14-1f60-11eb-a01a-bb528495b62f', 'system', '1458338bfbee9d2640aaea98f5e1547862cb4eb8e3070caf01cda260a84a391a', '_N9X9EbwC9fL', NULL, NULL),
									('77c81621-1f64-11eb-a01e-4716cf382111', 'VIP', '3abf4ce257be975dfdddef8d7bcc5f15ebab3c2e17a9f3f9364172817fe87d00', '@Wd2YLe+b+sm', NULL, '0f833e22-1f65-11eb-a020-e7c3a0389279'),
									('9b25487c-1f64-11eb-a01f-cf0ab5333201', 'drdonna', '5f6d254fee08c0f147e54656d74db2b779d233f6f835f7f898f88accbf5d25c0', 'PV@#7nfXZQ4D', '5b64bb4c5f3a2134a51148a7624c246f3e418c73902483a5aa8e0333eb48280a', '1dfccde2-1f65-11eb-a021-6701e091d585');
--SELECT * FROM uuid_generate_v1();


--CREATE SCHEMA IF NOT EXISTS "patientsimulator";

DROP TABLE IF EXISTS "patientsimulator"."organs" CASCADE;
CREATE TABLE "patientsimulator"."organs" (
  "uuid" uuid,
  "last_changed" timestamp DEFAULT (now()),
  PRIMARY KEY ("uuid")
);
INSERT INTO "patientsimulator"."organs" ("uuid", "last_changed") VALUES ('0536ADC8CC4944B79625964968187358','2020-06-02 06:43:43'),('0E2D7A5B75DF4879A479A9C228528BB8','2020-06-02 06:43:43'),('18F53F57B94043D890C5E7288EF2407D','2020-06-02 06:43:36'),('1C74C4319B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('1C76E66B9B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('1C78729A9B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('1C7A0C4D9B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('1C7B24949B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('1C7C43749B6011EA8D6798E7F42E1CCF','2020-05-21 14:39:28'),('2B4DC92969CB4688A7E9002DC522AA5D','2020-06-02 06:43:36'),('2DF1D809953F4F39BAFEDDA6EA0B7C6D','2020-06-02 06:43:43'),('31791BA27F294E4795AAD4A3534A8CB1','2020-06-02 06:43:36'),('40DCBC57E46A4C4A9619813B8C16E3DF','2020-06-02 06:45:57'),('416D0CD1CB0F447AAB7352B5EAD74C05','2020-06-02 06:45:57'),('5EF228AE3ECB4BC6892D08A131F158BB','2020-06-02 06:45:57'),('651767BAEB534012AB16E9AE1C35D38D','2020-06-02 06:43:44'),('679D21594B064E608B216636A54C4606','2020-06-02 06:43:36'),('6B32B094A6A744CD9AF0D66240611F31','2020-06-02 05:34:03'),('7B4A2B0F1ECC43D99AA1553AE16D717A','2020-06-02 05:37:18'),('844CCD2E9B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('844D6F359B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('844D70F19B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('844D71E09B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('844D73429B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('844D74829B6211EA8D6798E7F42E1CCF','2020-05-21 14:56:41'),('8491E09663B74078A6890B7D2006AE77','2020-06-02 05:32:28'),('84D868C6599946DB8E3E116FD7B3CAB1','2020-06-02 06:43:36'),('9AE930AF4B5B4026B41D018998E6D4BA','2020-06-02 06:45:57'),('A2602B03F3E1458185D307E737A8A3ED','2020-06-02 06:43:43'),('B4FF862AC9D84EB1A85926CC668E3B88','2020-06-02 06:43:36'),('BB504DCFA7FB412D9D253999568B9B2D','2020-06-02 06:43:44'),('BCE4551F99244E07B6AA6521301A599D','2020-06-02 06:45:57'),('BF18D01A9B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('BF1942F59B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('BF19443B9B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('BF19456E9B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('BF1945FE9B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('BF1946719B6111EA8D6798E7F42E1CCF','2020-05-21 14:51:10'),('C251F678BCE24ED5B60336D89DDC7FD3','2020-06-02 06:45:57'),('D51BC412EDCA4D269C4862857F6CF236','2020-06-02 06:41:32'),('D5D1FA2D56134B61BE1C83D98238D8AE','2020-06-02 05:35:50'),('DA298C39651A47079E996D7EE5307678','2020-06-02 05:38:52');
CREATE OR REPLACE FUNCTION update_timestamp() RETURNS trigger AS $$
BEGIN
	NEW."last_changed" = now();
	RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER update_last_changed_organs BEFORE UPDATE ON "patientsimulator"."organs"
 FOR EACH ROW EXECUTE PROCEDURE update_timestamp();

DROP TABLE IF EXISTS "patientsimulator"."vitalparameter_in_elga" CASCADE;
CREATE TABLE "patientsimulator"."vitalparameter_in_elga" (
  "code_parameter" varchar(9),
  "unit_parameter" varchar(10) DEFAULT NULL,
  "parameter_description" varchar(50) DEFAULT NULL,
  "value_set_parameter" varchar(50) DEFAULT NULL,
  "version_value_set" varchar(9) DEFAULT NULL,
  PRIMARY KEY ("code_parameter")
);
INSERT INTO "patientsimulator"."vitalparameter_in_elga" ("code_parameter", "unit_parameter", "parameter_description", "value_set_parameter", "version_value_set") VALUES ('2021-4','mm[Hg]','pCO2 ven.','ELGA_Laborparameter','4.4'),('2705-2','mm[Hg]','pO2 ven.','ELGA_Laborparameter','4.4'),('3141-9','kg','Body weight Measured','ELGA_Vitalparameterarten','4.0'),('32016-8','mg/dL','Glucose [Mass/volume] in Capillary blood','ELGA_Laborparameter','4.4'),('8302-2','cm','Body height','ELGA_Vitalparameterarten','4.0'),('8310-5','Cel','Body temperature','ELGA_Vitalparameterarten','4.0'),('8462-4','mm[Hg]','Diastolic blood pressure','ELGA_Vitalparameterarten','4.0'),('8478-0','mm[Hg]','Mean blood pressure','ELGA_Vitalparameterarten','4.0'),('8480-6','mm[Hg]','Systolic blood pressure','ELGA_Vitalparameterarten','4.0'),('9279-1','/min','Respiratory rate','ELGA_Vitalparameterarten','4.0');

DROP TABLE IF EXISTS "patientsimulator"."venous_blood_organ" CASCADE;
CREATE TABLE "patientsimulator"."venous_blood_organ" (
  "uuid" uuid NOT NULL,
  PRIMARY KEY ("uuid"),
  CONSTRAINT "venous_blood_organ_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."venous_blood_organ" ("uuid") VALUES ('18F53F57B94043D890C5E7288EF2407D'),('1C7B24949B6011EA8D6798E7F42E1CCF'),('2DF1D809953F4F39BAFEDDA6EA0B7C6D'),('5EF228AE3ECB4BC6892D08A131F158BB'),('844D6F359B6211EA8D6798E7F42E1CCF'),('BF1942F59B6111EA8D6798E7F42E1CCF');

DROP TABLE IF EXISTS "patientsimulator"."peripheral_vein_organ" CASCADE;
CREATE TABLE "patientsimulator"."peripheral_vein_organ" (
  "uuid" uuid NOT NULL,
  "venous_blood_uuid" uuid DEFAULT NULL,
  PRIMARY KEY ("uuid"),
--  KEY "venous_blood_uuid" ("venous_blood_uuid"),
  CONSTRAINT "peripheral_vein_organ_ibfk_1" FOREIGN KEY ("venous_blood_uuid") REFERENCES "patientsimulator"."venous_blood_organ" ("uuid"),
  CONSTRAINT "peripheral_vein_organ_ibfk_2" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid")
);
INSERT INTO "patientsimulator"."peripheral_vein_organ" ("uuid", "venous_blood_uuid") VALUES ('0536ADC8CC4944B79625964968187358',NULL),('1C7C43749B6011EA8D6798E7F42E1CCF','1C7B24949B6011EA8D6798E7F42E1CCF'),('844CCD2E9B6211EA8D6798E7F42E1CCF','844D6F359B6211EA8D6798E7F42E1CCF'),('84D868C6599946DB8E3E116FD7B3CAB1',NULL),('BCE4551F99244E07B6AA6521301A599D',NULL),('BF18D01A9B6111EA8D6798E7F42E1CCF','BF1942F59B6111EA8D6798E7F42E1CCF');

DROP TABLE IF EXISTS "patientsimulator"."peripheral_artery_organ" CASCADE;
CREATE TABLE "patientsimulator"."peripheral_artery_organ" (
  "uuid" uuid NOT NULL,
  PRIMARY KEY ("uuid"),
  CONSTRAINT "peripheral_artery_organ_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."peripheral_artery_organ" ("uuid") VALUES ('1C76E66B9B6011EA8D6798E7F42E1CCF'),('679D21594B064E608B216636A54C4606'),('844D73429B6211EA8D6798E7F42E1CCF'),('9AE930AF4B5B4026B41D018998E6D4BA'),('BB504DCFA7FB412D9D253999568B9B2D'),('BF1945FE9B6111EA8D6798E7F42E1CCF');

DROP TABLE IF EXISTS "patientsimulator"."lung_organ" CASCADE;
CREATE TABLE "patientsimulator"."lung_organ" (
  "uuid" uuid NOT NULL,
  PRIMARY KEY ("uuid"),
  CONSTRAINT "lung_organ_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."lung_organ" ("uuid") VALUES ('1C74C4319B6011EA8D6798E7F42E1CCF'),('2B4DC92969CB4688A7E9002DC522AA5D'),('40DCBC57E46A4C4A9619813B8C16E3DF'),('651767BAEB534012AB16E9AE1C35D38D'),('844D71E09B6211EA8D6798E7F42E1CCF'),('BF19456E9B6111EA8D6798E7F42E1CCF');

DROP TABLE IF EXISTS "patientsimulator"."capillary_blood_organ" CASCADE;
CREATE TABLE "patientsimulator"."capillary_blood_organ" (
  "uuid" uuid NOT NULL,
  PRIMARY KEY ("uuid"),
  CONSTRAINT "capillary_blood_organ_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."capillary_blood_organ" ("uuid") VALUES ('1C7A0C4D9B6011EA8D6798E7F42E1CCF'),('31791BA27F294E4795AAD4A3534A8CB1'),('416D0CD1CB0F447AAB7352B5EAD74C05'),('844D74829B6211EA8D6798E7F42E1CCF'),('A2602B03F3E1458185D307E737A8A3ED'),('BF1946719B6111EA8D6798E7F42E1CCF');

DROP TABLE IF EXISTS "patientsimulator"."body_organ" CASCADE;
CREATE TABLE "patientsimulator"."body_organ" (
  "uuid" uuid NOT NULL,
  PRIMARY KEY ("uuid"),
  CONSTRAINT "body_organ_ibfk_1" FOREIGN KEY ("uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."body_organ" ("uuid") VALUES ('0E2D7A5B75DF4879A479A9C228528BB8'),('1C78729A9B6011EA8D6798E7F42E1CCF'),('844D70F19B6211EA8D6798E7F42E1CCF'),('B4FF862AC9D84EB1A85926CC668E3B88'),('BF19443B9B6111EA8D6798E7F42E1CCF'),('C251F678BCE24ED5B60336D89DDC7FD3');

DROP TABLE IF EXISTS "patientsimulator"."continuous_vitalparameter" CASCADE;
CREATE TABLE "patientsimulator"."continuous_vitalparameter" (
  "organ_uuid" uuid NOT NULL,
  "vitalparameter_code" varchar(9) NOT NULL,
  "created_at" timestamp NOT NULL DEFAULT now(),
  "parameter_value" real DEFAULT NULL,
  PRIMARY KEY ("organ_uuid","vitalparameter_code","created_at"),
--  KEY "vitalparameter_code" ("vitalparameter_code"),
  CONSTRAINT "continuous_vitalparameter_ibfk_1" FOREIGN KEY ("organ_uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "continuous_vitalparameter_ibfk_2" FOREIGN KEY ("vitalparameter_code") REFERENCES "patientsimulator"."vitalparameter_in_elga" ("code_parameter") ON DELETE CASCADE
);

DROP TABLE IF EXISTS "patientsimulator"."reference_vitalparameter" CASCADE;
CREATE TABLE "patientsimulator"."reference_vitalparameter" (
  "organ_uuid" uuid NOT NULL,
  "vitalparameter_code" varchar(9) NOT NULL,
  "last_changed" timestamp DEFAULT (now()),
  "parameter_value" real DEFAULT NULL,
  PRIMARY KEY ("organ_uuid","vitalparameter_code"),
--  KEY "vitalparameter_code" ("vitalparameter_code"),
  CONSTRAINT "reference_vitalparameter_ibfk_1" FOREIGN KEY ("organ_uuid") REFERENCES "patientsimulator"."organs" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "reference_vitalparameter_ibfk_2" FOREIGN KEY ("vitalparameter_code") REFERENCES "patientsimulator"."vitalparameter_in_elga" ("code_parameter") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."reference_vitalparameter" ("organ_uuid", "vitalparameter_code", "last_changed", "parameter_value") VALUES ('0E2D7A5B75DF4879A479A9C228528BB8','3141-9','2020-06-02 06:43:44',100),('0E2D7A5B75DF4879A479A9C228528BB8','8302-2','2020-06-02 06:43:44',180),('0E2D7A5B75DF4879A479A9C228528BB8','8310-5','2020-06-02 06:43:44',37),('18F53F57B94043D890C5E7288EF2407D','2021-4','2020-06-02 06:43:36',356.279),('18F53F57B94043D890C5E7288EF2407D','2705-2','2020-06-02 06:43:36',322.526),('1C74C4319B6011EA8D6798E7F42E1CCF','9279-1','2020-05-21 15:46:31',12),('1C76E66B9B6011EA8D6798E7F42E1CCF','8462-4','2020-05-21 15:46:31',80),('1C76E66B9B6011EA8D6798E7F42E1CCF','8478-0','2020-05-21 15:46:31',93.333),('1C76E66B9B6011EA8D6798E7F42E1CCF','8480-6','2020-05-21 15:46:31',120),('1C78729A9B6011EA8D6798E7F42E1CCF','3141-9','2020-05-21 15:46:31',75),('1C78729A9B6011EA8D6798E7F42E1CCF','8302-2','2020-05-21 15:46:31',180),('1C78729A9B6011EA8D6798E7F42E1CCF','8310-5','2020-05-21 15:46:31',37),('1C7A0C4D9B6011EA8D6798E7F42E1CCF','32016-8','2020-05-21 15:46:31',90),('1C7B24949B6011EA8D6798E7F42E1CCF','2021-4','2020-05-21 15:46:31',356.279),('1C7B24949B6011EA8D6798E7F42E1CCF','2705-2','2020-05-21 15:46:31',322.526),('2B4DC92969CB4688A7E9002DC522AA5D','9279-1','2020-06-02 06:43:36',12),('2DF1D809953F4F39BAFEDDA6EA0B7C6D','2021-4','2020-06-02 06:43:43',356.279),('2DF1D809953F4F39BAFEDDA6EA0B7C6D','2705-2','2020-06-02 06:43:43',322.526),('31791BA27F294E4795AAD4A3534A8CB1','32016-8','2020-06-02 06:43:36',140),('40DCBC57E46A4C4A9619813B8C16E3DF','9279-1','2020-06-02 06:45:57',12),('416D0CD1CB0F447AAB7352B5EAD74C05','32016-8','2020-06-02 06:45:57',140),('5EF228AE3ECB4BC6892D08A131F158BB','2021-4','2020-06-02 06:45:57',356.279),('5EF228AE3ECB4BC6892D08A131F158BB','2705-2','2020-06-02 06:45:57',322.526),('651767BAEB534012AB16E9AE1C35D38D','9279-1','2020-06-02 06:43:44',12),('679D21594B064E608B216636A54C4606','8462-4','2020-06-02 06:43:36',95),('679D21594B064E608B216636A54C4606','8478-0','2020-06-02 06:43:36',113.333),('679D21594B064E608B216636A54C4606','8480-6','2020-06-02 06:43:36',150),('844D6F359B6211EA8D6798E7F42E1CCF','2021-4','2020-05-21 15:56:44',356.279),('844D6F359B6211EA8D6798E7F42E1CCF','2705-2','2020-06-02 05:52:03',50),('844D70F19B6211EA8D6798E7F42E1CCF','3141-9','2020-05-21 15:56:44',75),('844D70F19B6211EA8D6798E7F42E1CCF','8302-2','2020-05-21 15:56:44',180),('844D70F19B6211EA8D6798E7F42E1CCF','8310-5','2020-05-21 15:56:44',37),('844D71E09B6211EA8D6798E7F42E1CCF','9279-1','2020-05-21 15:56:44',12),('844D73429B6211EA8D6798E7F42E1CCF','8462-4','2020-05-21 15:56:44',80),('844D73429B6211EA8D6798E7F42E1CCF','8478-0','2020-05-21 15:56:44',93.333),('844D73429B6211EA8D6798E7F42E1CCF','8480-6','2020-05-21 15:56:44',120),('844D74829B6211EA8D6798E7F42E1CCF','32016-8','2020-06-01 21:50:58',70),('9AE930AF4B5B4026B41D018998E6D4BA','8462-4','2020-06-02 19:17:48',120),('9AE930AF4B5B4026B41D018998E6D4BA','8478-0','2020-06-02 06:45:58',113.333),('9AE930AF4B5B4026B41D018998E6D4BA','8480-6','2020-06-02 19:17:48',200),('A2602B03F3E1458185D307E737A8A3ED','32016-8','2020-06-02 06:43:43',140),('B4FF862AC9D84EB1A85926CC668E3B88','3141-9','2020-06-02 06:43:36',100),('B4FF862AC9D84EB1A85926CC668E3B88','8302-2','2020-06-02 06:43:36',180),('B4FF862AC9D84EB1A85926CC668E3B88','8310-5','2020-06-02 06:43:36',37),('BB504DCFA7FB412D9D253999568B9B2D','8462-4','2020-06-02 06:43:44',95),('BB504DCFA7FB412D9D253999568B9B2D','8478-0','2020-06-02 06:43:44',113.333),('BB504DCFA7FB412D9D253999568B9B2D','8480-6','2020-06-02 06:43:44',150),('BF1942F59B6111EA8D6798E7F42E1CCF','2021-4','2020-05-21 15:42:01',356.279),('BF1942F59B6111EA8D6798E7F42E1CCF','2705-2','2020-05-21 15:42:01',322.526),('BF19443B9B6111EA8D6798E7F42E1CCF','3141-9','2020-05-21 15:42:01',100),('BF19443B9B6111EA8D6798E7F42E1CCF','8302-2','2020-05-21 15:42:01',180),('BF19443B9B6111EA8D6798E7F42E1CCF','8310-5','2020-05-21 15:42:01',37),('BF19456E9B6111EA8D6798E7F42E1CCF','9279-1','2020-05-21 15:42:01',12),('BF1945FE9B6111EA8D6798E7F42E1CCF','8462-4','2020-05-21 15:42:01',95),('BF1945FE9B6111EA8D6798E7F42E1CCF','8478-0','2020-05-21 15:42:01',113.333),('BF1945FE9B6111EA8D6798E7F42E1CCF','8480-6','2020-05-21 15:42:01',150),('BF1946719B6111EA8D6798E7F42E1CCF','32016-8','2020-05-21 15:42:01',140),('C251F678BCE24ED5B60336D89DDC7FD3','3141-9','2020-06-02 19:17:05',90),('C251F678BCE24ED5B60336D89DDC7FD3','8302-2','2020-06-02 06:46:48',175),('C251F678BCE24ED5B60336D89DDC7FD3','8310-5','2020-06-02 06:45:57',37);
CREATE TRIGGER update_last_changed_referenceVitalparameter BEFORE UPDATE ON "patientsimulator"."reference_vitalparameter"
 FOR EACH ROW EXECUTE PROCEDURE update_timestamp();
 -- Function update_timestamp() is already created at this point.
--SELECT * from "patientsimulator"."reference_vitalparameter" as refere WHERE organ_uuid='c251f678-bce2-4ed5-b603-36d89ddc7fd3' AND vitalparameter_code='8302-2';
--UPDATE "patientsimulator"."reference_vitalparameter" SET parameter_value=170 WHERE organ_uuid='c251f678-bce2-4ed5-b603-36d89ddc7fd3' AND vitalparameter_code='8302-2';

DROP TABLE IF EXISTS "patientsimulator"."patient" CASCADE;
CREATE TABLE "patientsimulator"."patient" (
  "uuid" uuid NOT NULL,
  "created_at" timestamp DEFAULT (now()),
  "last_changed" timestamp DEFAULT (now()),
  "model_name" varchar(60) DEFAULT NULL,
  "gender" varchar(1) DEFAULT NULL,
  "owner_uuid" uuid,
  "rights_id" int NOT NULL,
  "is_alterable" boolean NOT NULL,
  "parent_uuid" uuid DEFAULT NULL,
  "peripheral_vein_uuid" uuid DEFAULT NULL,
  "venous_blood_uuid" uuid DEFAULT NULL,
  "capillary_blood_uuid" uuid DEFAULT NULL,
  "body_uuid" uuid DEFAULT NULL,
  "lung_uuid" uuid DEFAULT NULL,
  "peripheral_artery_uuid" uuid DEFAULT NULL,
  PRIMARY KEY ("uuid"),
--  KEY "parent_uuid" ("parent_uuid"),
--  KEY "peripheral_vein_uuid" ("peripheral_vein_uuid"),
--  KEY "venous_blood_uuid" ("venous_blood_uuid"),
--  KEY "capillary_blood_uuid" ("capillary_blood_uuid"),
--  KEY "body_uuid" ("body_uuid"),
--  KEY "lung_uuid" ("lung_uuid"),
--  KEY "peripheral_artery_uuid" ("peripheral_artery_uuid"),
  CONSTRAINT "patient_ibfk_1" FOREIGN KEY ("parent_uuid") REFERENCES "patientsimulator"."patient" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_2" FOREIGN KEY ("peripheral_vein_uuid") REFERENCES "patientsimulator"."peripheral_vein_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_3" FOREIGN KEY ("venous_blood_uuid") REFERENCES "patientsimulator"."venous_blood_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_4" FOREIGN KEY ("capillary_blood_uuid") REFERENCES "patientsimulator"."capillary_blood_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_5" FOREIGN KEY ("body_uuid") REFERENCES "patientsimulator"."body_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_6" FOREIGN KEY ("lung_uuid") REFERENCES "patientsimulator"."lung_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_7" FOREIGN KEY ("peripheral_artery_uuid") REFERENCES "patientsimulator"."peripheral_artery_organ" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_8" FOREIGN KEY ("owner_uuid") REFERENCES "management"."user" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "patient_ibfk_9" FOREIGN KEY ("rights_id") REFERENCES "management"."viewer_rights" ("id") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."patient" ("uuid", "created_at", "last_changed", "model_name", "gender", "owner_uuid", "rights_id", "is_alterable", "parent_uuid", "peripheral_vein_uuid", "venous_blood_uuid", "capillary_blood_uuid", "body_uuid", "lung_uuid", "peripheral_artery_uuid") 
	VALUES ('7A19253B9B6911EA8D6798E7F42E1CCF','2020-05-21 15:46:31','2020-05-21 15:46:31','Healthy Male Patient','m','78d72a14-1f60-11eb-a01a-bb528495b62f',1,'0',NULL,'1C7C43749B6011EA8D6798E7F42E1CCF','1C7B24949B6011EA8D6798E7F42E1CCF','1C7A0C4D9B6011EA8D6798E7F42E1CCF','1C78729A9B6011EA8D6798E7F42E1CCF','1C74C4319B6011EA8D6798E7F42E1CCF','1C76E66B9B6011EA8D6798E7F42E1CCF'),
			('C7A454DD9B6811EA8D6798E7F42E1CCF','2020-05-21 15:41:31','2020-05-21 15:41:31','Overweight Male Patient','m','78d72a14-1f60-11eb-a01a-bb528495b62f',1,'0',NULL,'BF18D01A9B6111EA8D6798E7F42E1CCF','BF1942F59B6111EA8D6798E7F42E1CCF','BF1946719B6111EA8D6798E7F42E1CCF','BF19443B9B6111EA8D6798E7F42E1CCF','BF19456E9B6111EA8D6798E7F42E1CCF','BF1945FE9B6111EA8D6798E7F42E1CCF'),
			('DC816B69A7C4472F9ECA47965B071EC4','2020-06-02 06:45:58','2020-10-19 13:10:53','Heinzl','m','77c81621-1f64-11eb-a01e-4716cf382111',2,'1','C7A454DD9B6811EA8D6798E7F42E1CCF','BCE4551F99244E07B6AA6521301A599D','5EF228AE3ECB4BC6892D08A131F158BB','416D0CD1CB0F447AAB7352B5EAD74C05','C251F678BCE24ED5B60336D89DDC7FD3','40DCBC57E46A4C4A9619813B8C16E3DF','9AE930AF4B5B4026B41D018998E6D4BA'),
			('E774CF7D9B6A11EA8D6798E7F42E1CCF','2020-05-21 15:56:44','2020-10-19 13:09:46','Margarete','w','9b25487c-1f64-11eb-a01f-cf0ab5333201',0,'1','7A19253B9B6911EA8D6798E7F42E1CCF','844CCD2E9B6211EA8D6798E7F42E1CCF','844D6F359B6211EA8D6798E7F42E1CCF','844D74829B6211EA8D6798E7F42E1CCF','844D70F19B6211EA8D6798E7F42E1CCF','844D71E09B6211EA8D6798E7F42E1CCF','844D73429B6211EA8D6798E7F42E1CCF');
--CREATE OR REPLACE FUNCTION update_timestamp() RETURNS trigger AS $$
--BEGIN
--	NEW."last_changed" = now();
--	RETURN NEW;
--END;
--$$ LANGUAGE plpgsql;
-- function already created above
CREATE TRIGGER update_last_changed_patient BEFORE UPDATE ON "patientsimulator"."patient"
 FOR EACH ROW EXECUTE PROCEDURE update_timestamp();

DROP TABLE IF EXISTS "patientsimulator"."physical_status_icd10" CASCADE;
CREATE TABLE "patientsimulator"."physical_status_icd10" (
  "code_icd10" varchar(7) NOT NULL,
  "version_year" int DEFAULT NULL,
  PRIMARY KEY ("code_icd10")
);
INSERT INTO "patientsimulator"."physical_status_icd10" ("code_icd10", "version_year") VALUES ('E11',2020),('E34.3',2020),('E66.0',2020),('I10.9',2020),('J00',2020);

DROP TABLE IF EXISTS "patientsimulator"."impairing_physical_change" CASCADE;
CREATE TABLE "patientsimulator"."impairing_physical_change" (
  "icd10_code" varchar(7) NOT NULL,
  "vitalparameter_code" varchar(9) NOT NULL,
  "ordinary_value" real DEFAULT NULL,
  "impaired_value" real DEFAULT NULL,
  PRIMARY KEY ("icd10_code","vitalparameter_code"),
--  KEY "vitalparameter_code" ("vitalparameter_code"),
  CONSTRAINT "impairing_physical_change_ibfk_1" FOREIGN KEY ("icd10_code") REFERENCES "patientsimulator"."physical_status_icd10" ("code_icd10") ON DELETE CASCADE,
  CONSTRAINT "impairing_physical_change_ibfk_2" FOREIGN KEY ("vitalparameter_code") REFERENCES "patientsimulator"."vitalparameter_in_elga" ("code_parameter") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."impairing_physical_change" ("icd10_code", "vitalparameter_code", "ordinary_value", "impaired_value") VALUES ('E11','32016-8',90,140),('E34.3','8302-2',180,145),('E66.0','3141-9',75,100),('E66.0','9279-1',12,15),('I10.9','8462-4',80,95),('I10.9','8478-0',93.333,113.333),('I10.9','8480-6',120,150),('J00','8310-5',37,38.5);

DROP TABLE IF EXISTS "patientsimulator"."alement_of_icd10" CASCADE;
CREATE TABLE "patientsimulator"."alement_of_icd10" (
  "patient_uuid" uuid NOT NULL,
  "icd10_code" varchar(7) NOT NULL,
  PRIMARY KEY ("patient_uuid","icd10_code"),
--  KEY "icd10_code" ("icd10_code"),
  CONSTRAINT "alement_of_icd10_ibfk_1" FOREIGN KEY ("patient_uuid") REFERENCES "patientsimulator"."patient" ("uuid") ON DELETE CASCADE,
  CONSTRAINT "alement_of_icd10_ibfk_2" FOREIGN KEY ("icd10_code") REFERENCES "patientsimulator"."physical_status_icd10" ("code_icd10") ON DELETE CASCADE
);
INSERT INTO "patientsimulator"."alement_of_icd10" ("patient_uuid", "icd10_code") VALUES ('C7A454DD9B6811EA8D6798E7F42E1CCF','E11'),('C7A454DD9B6811EA8D6798E7F42E1CCF','E66.0'),('C7A454DD9B6811EA8D6798E7F42E1CCF','I10.9');

